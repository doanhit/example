package com.zoobie;

import com.facebook.react.ReactActivity;

import cl.json.ShareApplication;

public class MainActivity extends ReactActivity implements ShareApplication{

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Zoobie";
    }
    @Override
     public String getFileProviderAuthority() {
            return "com.example.com.zoombie.provider";
     }
}
