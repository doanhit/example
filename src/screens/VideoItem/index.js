import React, { Component } from 'react'
import {
    Text,
    View,
    WebView,TouchableOpacity
} from 'react-native'
import styles from './styles'
import { Button } from 'react-native-elements'
import ViewMoreText from 'react-native-view-more-text';
export default class VideoItem extends Component {
    renderViewMore(onPress){
        return(
          <Text onPress={onPress}>View more</Text>
        )
      };
      renderViewLess(onPress){
        return(
          <Text onPress={onPress}>View less</Text>
        )
      };
      myPress = () => {
        this.props.onPressItemVideo(this.props.title, this.props.url);
      }
    render() {
        const { title, url, createdAt } = this.props;
        return (

            <View style={styles.container}>
                <View style={{ width: '40%', height: 100, backgroundColor: 'green'}}>
                    <WebView source={{uri: url}}/>
                </View>
                <View style={styles.contentview}>
                <TouchableOpacity  onPress={this.myPress}> 
                    <ViewMoreText
                        numberOfLines={1}
                        renderViewMore={this.renderViewMore}
                        renderViewLess={this.renderViewLess}
                       
                    >
                    
                        <Text style={styles.title}  >
                            {title}
                        </Text>
                    </ViewMoreText>
                </TouchableOpacity>
                    <View style={[{
                        marginTop: 10, position: 'absolute',
                        bottom: 0,
                        left: 0,
                       
                    }]}>
                        {/* <Button
                            title="Detail"
                            onPress={this.myPress}
                            titleStyle={{ fontWeight: "700" }}
                            buttonStyle={{
                                backgroundColor: "0f3443",
                                width: 80,
                                height: 20,
                                borderColor: "transparent",
                                borderWidth: 2,
                                borderRadius: 5
                                
                            }} /> */}
                    </View>
                    
                </View>
               
            </View>
            
        )
    }
}