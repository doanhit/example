import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        // marginLeft:30,
        marginBottom:20,
        marginRight: 30,
        width: 370,
        flex: 2,
        flexDirection: 'row',
        borderBottomColor: 'white',
        borderBottomWidth: 1,


    },
    contentview: {
        marginLeft: 30,
        width: '60%',
        height: 100,
        marginBottom: 30,
        // backgroundColor:'#E5E7E9',
        borderRadius: 5,
    },
    // content: {
    //     width: "50%",
    //     marginLeft: 10,
    //     marginTop: 20,
    //     backgroundColor: "green"
    // },
    title: {

        // marginLeft: 5
        color: 'white',
        fontSize: 18,
        // fontWeight: 'bold',
        // alignContent: 'center',
    },
})
export default styles