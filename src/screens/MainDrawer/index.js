import React, { PureComponent } from 'react'
import {
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image
} from 'react-native'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Icon from "react-native-vector-icons/FontAwesome"


import styles from './styles'

class Drawer extends PureComponent {
  static propTypes = {
    drawerProps: PropTypes.object.isRequired
  }

  closeDawer = () => {
    const { drawerProps } = this.props
    drawerProps.navigation.closeDrawer()
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerBlock}>
          <View style={styles.closeContain}>
            <TouchableOpacity style={styles.touchClose} onPress={this.closeDawer}>

              <Image style={styles.iconClose} source={require('../../../assets/images/close-drawer.png')} />
            </TouchableOpacity>
          </View>
          <View style={styles.viewlogoEnglish}>
          <Image style={styles.logoEnglish}  source={require('../../../assets/images/eng_logo.png')} />
          </View>
        </View>
        <View style={styles.menuBlock}>
          <ScrollView style={styles.menuContain}>

            <TouchableOpacity style={styles.touchMenuItem}>
              <Text style={styles.menuItemText}>
              Home
              <Text >{"     "}</Text>
              <Text >{"     "}</Text>
              <Icon style= {styles.iconmenu} name="home" size={30} color="#959191" />
              </Text>

            </TouchableOpacity>

            <TouchableOpacity style={styles.touchMenuItem}>
              <Text style={styles.menuItemText}>
              Trending
              <Text >{"     "}</Text>
              <Text >{"     "}</Text>
              <Icon style= {styles.iconmenu} name="fire" size={30} color="#959191" />
              </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.touchMenuItem}>
              <Text style={styles.menuItemText}>
              Popular
              <Text >{"     "}</Text>
              <Text >{"     "}</Text>
              <Icon style= {styles.iconmenu} name="globe" size={30} color="#959191" />
              </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.touchMenuItem}>
              <Text style={styles.menuItemText}>
              TOEIC
              <Text >{"     "}</Text>
              <Text >{"     "}</Text>
              <Icon style= {styles.iconmenu} name="book" size={30} color="#959191" />
              </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.touchMenuItem}>
              <Text style={styles.menuItemText}>
              IELTS
              <Text >{"     "}</Text>
              <Text >{"     "}</Text>
              <Icon style= {styles.iconmenu} name="pencil-square" size={30} color="#959191" />
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>

        <View style={styles.footerBlock}>
          <Text style={styles.versionText}>Version 2.10 (2018)</Text>
        </View>
      </View>
    )
  }
}

export default connect(
  null,
  null
)(Drawer)
