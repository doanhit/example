import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,
  Image
} from 'react-native';
import YouTube, { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';

import GridView from 'react-native-super-grid';

export default class Example extends Component {
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };

  render() {
    // Taken from https://flatuicolors.com/
    const items = [
      { name: 'TURQUOISE', code: '#1abc9c' },
      { name: 'TURQUOISE', code: '#1abc9c' },
      { name: 'TURQUOISE', code: '#1abc9c' },
      { name: 'TURQUOISE', code: '#1abc9c' },
      { name: 'TURQUOISE', code: '#1abc9c' },
      { name: 'TURQUOISE', code: '#1abc9c' },
      { name: 'TURQUOISE', code: '#1abc9c' },

    ];

    return (
      <View style = {styles.container}>
          <View style = {styles.up}>
            <View style={[styles.screenVideo, { backgroundColor: 'red' }]}>
            <ScrollView
              style={styles.container}
              onLayout={({ nativeEvent: { layout: { width } } }) => {
                if (!this.state.containerMounted) this.setState({ containerMounted: true });
                if (this.state.containerWidth !== width) this.setState({ containerWidth: width });
              }}
            >

              {this.state.containerMounted && (
                <YouTube
                  ref={component => {
                    this._youTubeRef = component;
                  }}
                  // You must have an API Key for the player to load in Android
                  apiKey="AIzaSyDxA16CXHAi1Y0gwBYktWhhbvtyC4hOY_0"
                  // Un-comment one of videoId / videoIds / playlist.
                  // You can also edit these props while Hot-Loading in development mode to see how
                  // it affects the loaded native module
                  videoId="4y3caO8KOrM"
                  // videoIds={['HcXNPI-IPPM', 'XXlZfc1TrD0', 'czcjU1w-c6k', 'uMK0prafzw0']}
                  // playlistId="PLF797E961509B4EB5"
                  play={this.state.isPlaying}
                  loop={this.state.isLooping}
                  fullscreen={this.state.fullscreen}
                  controls={1}
                  style={[
                    { height: PixelRatio.roundToNearestPixel(this.state.containerWidth / (16 / 9)) },
                    styles.player,
                  ]}
                  onError={e => this.setState({ error: e.error })}
                  onReady={e => this.setState({ isReady: true })}
                  onChangeState={e => this.setState({ status: e.state })}
                  onChangeQuality={e => this.setState({ quality: e.quality })}
                  onChangeFullscreen={e => this.setState({ fullscreen: e.isFullscreen })}
                  onProgress={e => this.setState({ duration: e.duration, currentTime: e.currentTime })}
                />
              )}


            </ScrollView>
            </View>
          </View>
          <View style={{flex: 0.6, flexDirection: 'row', paddingLeft: 50}}>
            <Text>Học ngữ pháp thi đờn giản      </Text>
            <Image style={styles.imageStyle}
             source={{uri: 'https://cungdev.com/wp-content/uploads/2018/02/avatar-300x300.jpg'}}>
            </Image>
            <Image style={styles.imageStyle}
             source={{uri: 'https://cungdev.com/wp-content/uploads/2018/02/avatar-300x300.jpg'}}>
            </Image>
      </View>
          <View style = {styles.down}>
            <GridView
                itemDimension={130}
                horizontal ={true}
                items={items}
                style={styles.gridView}
                renderItem={item => (
            <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
              <Text style={styles.itemName}>{item.name}</Text>
              <Text style={styles.itemCode}>{item.code}</Text>
            </View>
        )}
      />
          </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  up:{
    flex: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  down: {
    flex: 5
  },
  gridView: {
    paddingTop: 5,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'center',
    borderRadius: 5,
    padding: 10,
    height: 150,
    width: 250
  },
  screenVideo:{
    justifyContent: 'center',
    borderRadius: 5,
    padding: 100,
    height: 150,
    width: 250,

  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  imageStyle:{
    width: 30, height: 30,
    marginBottom:20,
    marginLeft:10,
    borderTopLeftRadius:10,
    borderTopRightRadius:10,
    borderBottomLeftRadius:10,
    borderBottomRightRadius:10
  },
});
