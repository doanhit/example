import React, { Component } from 'react'
import { Text,Alert,Button,
    BackAndroid, Video, WebView,TouchableHighlight,BackHandler,ToastAndroid, View, Image, StyleSheet, Dimensions } from 'react-native'
import styles from './style';
import GridView from 'react-native-super-grid';
import CardVideo from "./../VideoItem/index";
import { SearchBar } from 'react-native-elements'
import Carousel from 'react-native-looped-carousel';
let backHandlerClickCount = 0;
import LinearGradient from 'react-native-linear-gradient';
export default class ViewVideo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            dataSource: [],
            visible: 4,
        };
        this.loadMore = this.loadMore.bind(this);
    }
    loadMore() {
        this.setState((prev) => {
          return {visible: prev.visible + 4};
        });
      }
    componentDidMount() {
        return fetch('http://www.json-generator.com/api/json/get/cedBRaARAi?indent=2')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson,
                })
               
            })
            .catch((error) => {
                console.log(error)
            });
    }
    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ size: { width: layout.width, height: layout.height } });
    };

    _onPressItemVideo = (title, url) => {
        const { navigation } = this.props;
        navigation.navigate('ViewVideo', { titleVideo: title, urlVideo: url });
    }

    render() {
        BackHandler.addEventListener('hardwareBackPress', function() {
            Alert.alert(
              'Exit the app',
              'Do you want to exit the app?', [{
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel'
              }, {
                  text: 'OK',
                  onPress: () => BackHandler.exitApp()
              }, ], {
                  cancelable: false
              }
           )
           return true;
        })
        return (
            <LinearGradient
            start={{x: 0, y: 1}} end={{x: 1, y: 0}}
            colors={['#34e89e','#0f3443']}
            >
            <View style = { styles.container } >

                <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
                    {
                        !this.state.dataSource.length && (
                            <Text>loading....</Text>
                        )
                    }
                    {
                        !!this.state.dataSource.length && (
                     
                      <View style={{flex: 1}}>

                      <View style={{flex: 5,marginTop:10}}>
                            <Carousel style={{ height: 210}}
                                        delay={2000}                                   
                                        style={this.state.size}      
                                        isLooped={false}
                                        autoplay={false}
                                    >
                                        {this.state.dataSource.map((item, i) => 
                                        <View key={item.id} style={{ flex: 1, flexDirection: 'column', height: 200, marginLeft: 10,marginRight: 10,backgroundColor: 'green'}}>
                                        <WebView bounces style={{borderWidth:1, borderRadius:20}}  source ={{uri: item.url}} /></View>)}
                            </Carousel>
                        </View>

                        <View style={{flex: 10}}>

                            <GridView
                            itemDimension={330}
                            items={this.state.dataSource.slice(0, this.state.visible)}
                            style={styles.gridView}
                            renderItem={(item, itemIndex) => (
                            <View>

                            <CardVideo title = {item.title} key={item.id}  onPressItemVideo={() => this._onPressItemVideo(item.title, item.url)}
                             url = {item.url}/>
                              {this.state.visible < this.state.dataSource.length && itemIndex == this.state.visible-1 &&
                                <View style={styles.loadMoreView}>
                                    <TouchableHighlight 
                                        style ={{
                                            width: '100%',
                                            height: 25,
                                            marginTop:20,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            borderRadius: 5
                                        }}>
                                <Button
                                color="green"
                                title="Load more"
                                onPress={this.loadMore}
                                titleStyle={{ fontWeight: "700" }}
                                style = {styles.btnLoadmore} /></TouchableHighlight></View>
                             }
                             </View>
                            )
                           }
                            />
                        
                        
                      </View>                            
                       
                     
                    </View>
                   
                    
                        )
                    }
                    
                </View>
              
            </View>
        
</LinearGradient>
        );


    }
}