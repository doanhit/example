import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    // backgroundColor: '#3cb371',
    height: '100%',
    width:'100%',
  },
  container_search:{
    marginTop:20,
    justifyContent: 'center',
    marginHorizontal: 40,
    
  },
  loadMoreView:{

    width:380,
    flex: 2,
    flexDirection: 'row', 
  },

      searchbar:{
        width: '100%',
        marginLeft: 0, 
        marginRight: 0,
        alignItems: "center",
        alignSelf: 'stretch',
      },
      gridView:{
        borderRadius: 5,
      },
      text:{
        color:'green',
        fontSize:20,
        marginLeft:10,
        marginBottom: 10,


      }

});
export default styles