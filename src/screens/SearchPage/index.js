import React, { Component } from 'react'

import { Text,Alert,Button, Video, WebView,TouchableHighlight, View, Image, StyleSheet, Dimensions } from 'react-native'
import styles from '../Video/style';
import GridView from 'react-native-super-grid';
import CardVideo from "./../VideoItem/index";
import { SearchBar } from 'react-native-elements'
import Carousel from 'react-native-looped-carousel';

import LinearGradient from 'react-native-linear-gradient';
export default class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            dataSource: [],
            visible: 4,
        };
        this.arrayholder = [];
        this.loadMore = this.loadMore.bind(this);
    }
    loadMore() {
        this.setState((prev) => {
          return {visible: prev.visible + 4};
        });
      }
    componentDidMount() {
        return fetch('http://www.json-generator.com/api/json/get/cedBRaARAi?indent=2')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    isLoading: false,
                    dataSource: responseJson,
                })
                this.arrayholder = responseJson;
            })
            .catch((error) => {
                console.log(error)
            });
    }
    _onLayoutDidChange = (e) => {
        const layout = e.nativeEvent.layout;
        this.setState({ size: { width: layout.width, height: layout.height } });
    };

    _onPressItemVideo = (title, url) => {
        const { navigation } = this.props;
        navigation.navigate('ViewVideo', { titleVideo: title, urlVideo: url });
    }
    searchFilterFunction = text => {
        console.log(this.arrayholder);
        const newData = this.arrayholder.filter(item => {
            const itemData = `${item.title.toUpperCase()} `;
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        this.setState({
            dataSource: newData,
        });
    };


    render() {
        return (
            <LinearGradient
            start={{x: 0, y: 1}} end={{x: 1, y: 0}}
            colors={['#34e89e','#0f3443']}
            >
            <View style = { styles.container } >
            <View style={styles.container_search}>
                    <SearchBar
                        round
                        style={styles.searchbar}
                        inputStyle={{ backgroundColor: 'white', borderWidth: 1.3, borderColor: '#000000' }}
                        containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, borderTopWidth: 0, borderBottomWidth: 0 }}
                        lightTheme
                        icon={{ type: 'font-awesome', name: 'search' }}
                        onChangeText={text => this.searchFilterFunction(text)}
                        autoCorrect={false}
                        placeholder='write sth...' />

                </View>

                <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>
                    {
                        !this.state.dataSource.length && (
                            <Text>loading....</Text>
                        )
                    }
                    {
                        !!this.state.dataSource.length && (
                      <View style={{flex: 1}}>
                        <View style={{flex: 2}}>

                            <GridView
                            itemDimension={330}
                            items={this.state.dataSource}
                            style={styles.gridView}
                            renderItem={item => (
                                
                            <CardVideo title = {item.title}  onPressItemVideo={() => this._onPressItemVideo(item.title, item.url)} url = {item.url}/>
                            )}
                            />
                      </View>

                    </View>
                        )
                    }
                </View>
               
            </View>
</LinearGradient>
        );


    }
}
